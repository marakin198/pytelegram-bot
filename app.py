import logging
from aiogram.types import ReplyKeyboardRemove, \
    ReplyKeyboardMarkup, KeyboardButton, \
    InlineKeyboardMarkup, InlineKeyboardButton
from config import API_TOKEN 
from contextvars import Token
from aiogram import Bot, types, dispatcher, executor
from aiogram.dispatcher import Dispatcher
from aiogram.utils import executor


logging.basicConfig(level=logging.INFO)
bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)

#test
button_hi = KeyboardButton('Валюта')

greet_kb = ReplyKeyboardMarkup()
greet_kb.add(button_hi)

@dp.message_handler(commands=['start', 'help'])
async def send_welcome(message: types.Message):
    await message.reply("Привет!\n Я Taxestestbot!\nСделан Валерой.", reply_markup=greet_kb)


@dp.message_handler()
async def echo(message: types.Message):
    if message.text == 'Валюта':
        await message.answer('ETH/BNB')

if __name__ == '__main__':
    executor.start_polling(dp)


    
